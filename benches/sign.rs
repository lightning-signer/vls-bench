#[macro_use]
extern crate bencher;

use bencher::{Bencher, black_box};
use secp256k1::{KeyPair, Message, PublicKey, SecretKey, XOnlyPublicKey};

fn sign_ecdsa(b: &mut Bencher) {
    let secret = SecretKey::from_slice(&[1; 32]).unwrap();
    let secp = secp256k1::Secp256k1::new();
    let message = Message::from_slice(&[3; 32]).unwrap();
    b.iter(|| {
        let sig = secp.sign_ecdsa(&message, &secret);
        black_box(sig);
    });
}

fn verify_ecdsa(b: &mut Bencher) {
    let secret = SecretKey::from_slice(&[1; 32]).unwrap();
    let secp = secp256k1::Secp256k1::new();
    let pubkey = PublicKey::from_secret_key(&secp, &secret);
    let message = Message::from_slice(&[3; 32]).unwrap();
    let sig = secp.sign_ecdsa(&message, &secret);
    b.iter(|| {
        let res = secp.verify_ecdsa(&message, &sig, &pubkey).unwrap();
        black_box(res);
    });
}

fn sign_schnorr(b: &mut Bencher) {
    let secret = SecretKey::from_slice(&[1; 32]).unwrap();
    let secp = secp256k1::Secp256k1::new();
    let keypair = KeyPair::from_secret_key(&secp, &secret);
    let message = Message::from_slice(&[3; 32]).unwrap();
    b.iter(|| {
        let sig = secp.sign_schnorr_no_aux_rand(&message, &keypair);
        black_box(sig);
    });
}

fn verify_schnorr(b: &mut Bencher) {
    let secret = SecretKey::from_slice(&[1; 32]).unwrap();
    let secp = secp256k1::Secp256k1::new();
    let keypair = KeyPair::from_secret_key(&secp, &secret);
    let xonly_pubkey = XOnlyPublicKey::from_keypair(&keypair).0;
    let message = Message::from_slice(&[3; 32]).unwrap();
    let sig = secp.sign_schnorr_no_aux_rand(&message, &keypair);
    b.iter(|| {
        let res = secp.verify_schnorr(&sig, &message, &xonly_pubkey).unwrap();
        black_box(res);
    });
}

benchmark_group!(benches, sign_ecdsa, verify_ecdsa, sign_schnorr, verify_schnorr);
benchmark_main!(benches);
