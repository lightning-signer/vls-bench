# VLS Benchmark Suite

This repo benchmarks various crypto primitives.

## Results

More than 10,000 sign+verify operations per second per CPU core.

```
running 4 tests
test sign_ecdsa     ... bench:      25,573 ns/iter (+/- 160)
test sign_schnorr   ... bench:      20,165 ns/iter (+/- 217)
test verify_ecdsa   ... bench:      30,870 ns/iter (+/- 350)
test verify_schnorr ... bench:      31,927 ns/iter (+/- 321)
```
